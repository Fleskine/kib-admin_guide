Dashboard
=========
This user interface aims at providing a summary of most entities that interact with the system. Reporting the states of various system components is enhanced with visualizations such as bar graphs and charts. Inspecting any of the components renders a bar graph and a pie chart on the lower left and right side respectively. The illustrations below provide a summary of innovations.


    ..  figure::  ../../images/SummaryCards.png
        :class: with-border
        :alt: System components
        :scale: 80
        :align: center

        *Figure 1.1: State summary of system components*

        
    ..  figure::  ../../images/InnovationsGraphChart.png
        :class: with-border
        :alt: Bar graph & pie chart of innovations
        :scale: 80
        :align: center

        *Figure 1.2: Summary plots of innovations*