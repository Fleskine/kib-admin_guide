���:      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Events�h]�h	�Text����Events�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�9C:\Sphinx_Learning\kib-admin-guide\docs\source\events.rst�hKubh	�	paragraph���)��}�(h�OThis module bears events organized to foster innovations as is the goal of KIB.�h]�h�OThis module bears events organized to foster innovations as is the goal of KIB.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�Existing Events�h]�h�Existing Events�����}�(hh@hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh=hhhh,hKubh.)��}�(h�rBy default, navigating to this module renders a table of current events on KIB. These events are categorized into:�h]�h�rBy default, navigating to this module renders a table of current events on KIB. These events are categorized into:�����}�(hhNhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh=hhubh	�enumerated_list���)��}�(hhh]�(h	�	list_item���)��}�(h�LOpen Events: These are events that are upcoming and are open to invitations.�h]�h.)��}�(hheh]�h�LOpen Events: These are events that are upcoming and are open to invitations.�����}�(hhghhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK	hhcubah}�(h!]�h#]�h%]�h']�h)]�uh+hahh^hhhh,hNubhb)��}�(h��Closed Events: These are events that are no longer open to invitations.

 ..  figure::  ../../images/EventsTable.png
     :class: with-border
     :alt: Logs
     :align: center

     *Figure 36: Table of events*

�h]�(h.)��}�(h�GClosed Events: These are events that are no longer open to invitations.�h]�h�GClosed Events: These are events that are no longer open to invitations.�����}�(hh~hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK
hhzubh	�block_quote���)��}�(h��..  figure::  ../../images/EventsTable.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 36: Table of events*

�h]�h	�figure���)��}�(hhh]�(h	�image���)��}�(h��..  figure::  ../../images/EventsTable.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 36: Table of events*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/EventsTable.png��
candidates�}��*�h�suh+h�hh�hh,hK ubh	�caption���)��}�(h�*Figure 36: Table of events*�h]�h	�emphasis���)��}�(hh�h]�h�Figure 36: Table of events�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhh�ubeh}�(h!]��id1�ah#]�h%]�h']�h)]��align��center�uh+h�hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhhzubeh}�(h!]�h#]�h%]�h']�h)]�uh+hahh^hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]��enumtype��arabic��prefix�h�suffix��.�uh+h\hh=hhhh,hK	ubeh}�(h!]��existing-events�ah#]�h%]��existing events�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Creating New Events�h]�h�Creating New Events�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubh.)��}�(hX  To create an upcoming event, select the 'Add Event' option located on the top right of the Events table. This provides a form with inputs corresponding to the details required to establish an upcoming event. Depicted below is a snippet of the input form provided for this cause.�h]�hX  To create an upcoming event, select the ‘Add Event’ option located on the top right of the Events table. This provides a form with inputs corresponding to the details required to establish an upcoming event. Depicted below is a snippet of the input form provided for this cause.�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�hhubh�)��}�(h��..  figure::  ../../images/AddEvent.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 37: Creating an upcoming event*

�h]�h�)��}�(hhh]�(h�)��}�(h��..  figure::  ../../images/AddEvent.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 37: Creating an upcoming event*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/AddEvent.png�h�}�h�j"  suh+h�hj  hh,hK ubh�)��}�(h�'*Figure 37: Creating an upcoming event*�h]�h�)��}�(hj&  h]�h�%Figure 37: Creating an upcoming event�����}�(hj(  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj$  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhj  ubeh}�(h!]��id2�ah#]�h%]�h']�h)]�h͌center�uh+h�hKhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKhh�hhubeh}�(h!]��creating-new-events�ah#]�h%]��creating new events�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Activating/Deactivating Events�h]�h�Activating/Deactivating Events�����}�(hjT  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjQ  hhhh,hK!ubh.)��}�(hX�  Once an upcoming event has been created, it needs to be set to an active state for it to be open to invitations.
- To activate a specific event, click the orange toggle button in the Actions column of the table. The toggle button turns green to signify that the event has been activated. Conversely, to deactivate a specific event, click the green toggle button. This will turn it back to orange to signify an inactive state.�h]�hX�  Once an upcoming event has been created, it needs to be set to an active state for it to be open to invitations.
- To activate a specific event, click the orange toggle button in the Actions column of the table. The toggle button turns green to signify that the event has been activated. Conversely, to deactivate a specific event, click the green toggle button. This will turn it back to orange to signify an inactive state.�����}�(hjb  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK"hjQ  hhubh�)��}�(h��..  figure::  ../../images/ActivateCall.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 38: Activating/Deactivating an event*

�h]�h�)��}�(hhh]�(h�)��}�(h��..  figure::  ../../images/ActivateCall.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 38: Activating/Deactivating an event*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/ActivateCall.png�h�}�h�j�  suh+h�hjt  hh,hK ubh�)��}�(h�-*Figure 38: Activating/Deactivating an event*�h]�h�)��}�(hj�  h]�h�+Figure 38: Activating/Deactivating an event�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK*hjt  ubeh}�(h!]��id3�ah#]�h%]�h']�h)]�h͌center�uh+h�hK*hjp  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK%hjQ  hhubeh}�(h!]��activating-deactivating-events�ah#]�h%]��activating/deactivating events�ah']�h)]�uh+h
hhhhhh,hK!ubh)��}�(hhh]�(h)��}�(h�Updating Events�h]�h�Updating Events�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK.ubh.)��}�(h��To modify an established event on KIB, select the pen-like icon against the event of interest. Modify the input fields in the resulting form as needed and then select the 'save' option at the bottom right.�h]�h��To modify an established event on KIB, select the pen-like icon against the event of interest. Modify the input fields in the resulting form as needed and then select the ‘save’ option at the bottom right.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK/hj�  hhubh�)��}�(hX  ..  figure::  ../../images/UpdateCall.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 39: Update icon*


..  figure::  ../../images/EventUpdateForm.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 40: Form to update an event*

�h]�(h�)��}�(hhh]�(h�)��}�(h��..  figure::  ../../images/UpdateCall.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 39: Update icon*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/UpdateCall.png�h�}�h�j�  suh+h�hj�  hh,hK ubh�)��}�(h�*Figure 39: Update icon*�h]�h�)��}�(hj�  h]�h�Figure 39: Update icon�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK6hj�  ubeh}�(h!]��id4�ah#]�h%]�h']�h)]�h͌center�uh+h�hK6hj�  ubh�)��}�(hhh]�(h�)��}�(h��..  figure::  ../../images/EventUpdateForm.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 40: Form to update an event*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri�� ../../images/EventUpdateForm.png�h�}�h�j  suh+h�hj	  hh,hK ubh�)��}�(h�$*Figure 40: Form to update an event*�h]�h�)��}�(hj  h]�h�"Figure 40: Form to update an event�����}�(hj   hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK>hj	  ubeh}�(h!]��id5�ah#]�h%]�h']�h)]�h͌center�uh+h�hK>hj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK1hj�  hhubeh}�(h!]��updating-events�ah#]�h%]��updating events�ah']�h)]�uh+h
hhhhhh,hK.ubh)��}�(hhh]�(h)��}�(h�Deleting Events�h]�h�Deleting Events�����}�(hjL  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjI  hhhh,hKBubh.)��}�(h�To delete a specific event from the table of events, select the bin-like icon against the specific event in the Actions column.�h]�h�To delete a specific event from the table of events, select the bin-like icon against the specific event in the Actions column.�����}�(hjZ  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKChjI  hhubh�)��}�(h��..  figure::  ../../images/DelUser.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 41: Deletion icon*

�h]�h�)��}�(hhh]�(h�)��}�(h��..  figure::  ../../images/DelUser.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 41: Deletion icon*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/DelUser.png�h�}�h�j}  suh+h�hjl  hh,hK ubh�)��}�(h�*Figure 41: Deletion icon*�h]�h�)��}�(hj�  h]�h�Figure 41: Deletion icon�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKJhjl  ubeh}�(h!]��id6�ah#]�h%]�h']�h)]�h͌center�uh+h�hKJhjh  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKEhjI  hhubeh}�(h!]��deleting-events�ah#]�h%]��deleting events�ah']�h)]�uh+h
hhhhhh,hKBubh)��}�(hhh]�(h)��}�(h�Broadcasting Events�h]�h�Broadcasting Events�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKNubh.)��}�(h�ZTo broadcast a specific event, select the megaphone icon against it in the Actions column.�h]�h�ZTo broadcast a specific event, select the megaphone icon against it in the Actions column.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKOhj�  hhubh�)��}�(h��..  figure::  ../../images/BroadcastIcon.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 42: Broadcasting icon*

�h]�h�)��}�(hhh]�(h�)��}�(h��..  figure::  ../../images/BroadcastIcon.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 42: Broadcasting icon*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/BroadcastIcon.png�h�}�h�j�  suh+h�hj�  hh,hK ubh�)��}�(h�*Figure 42: Broadcasting icon*�h]�h�)��}�(hj�  h]�h�Figure 42: Broadcasting icon�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKVhj�  ubeh}�(h!]��id7�ah#]�h%]�h']�h)]�h͌center�uh+h�hKVhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKQhj�  hhubeh}�(h!]��broadcasting-events�ah#]�h%]��broadcasting events�ah']�h)]�uh+h
hhhhhh,hKNubh)��}�(hhh]�(h)��}�(h�Events Logs�h]�h�Events Logs�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj  hhhh,hKZubh.)��}�(h�BTo view the logs of a specific event, select the scroll-like icon.�h]�h�BTo view the logs of a specific event, select the scroll-like icon.�����}�(hj   hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK[hj  hhubh�)��}�(h�|..  figure::  ../../images/LogIcon.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 43: Logs icon*�h]�h�)��}�(hhh]�(h�)��}�(h�|..  figure::  ../../images/LogIcon.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 43: Logs icon*�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/LogIcon.png�h�}�h�jC  suh+h�hj2  hh,hK ubh�)��}�(h�*Figure 43: Logs icon*�h]�h�)��}�(hjG  h]�h�Figure 43: Logs icon�����}�(hjI  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjE  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hKbhj2  ubeh}�(h!]��id8�ah#]�h%]�h']�h)]�h͌center�uh+h�hKbhj.  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh,hK]hj  hhubeh}�(h!]��events-logs�ah#]�h%]��events logs�ah']�h)]�uh+h
hhhhhh,hKZubeh}�(h!]��events�ah#]�h%]��events�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(jw  jt  h�h�jN  jK  j�  j�  jF  jC  j�  j�  j  j	  jo  jl  u�	nametypes�}�(jw  �h�jN  �j�  �jF  �j�  �j  �jo  �uh!}�(jt  hh�h=jK  h�j�  jQ  jC  j�  j�  jI  j	  j�  jl  j  h�h�j=  j  j�  jt  j  j�  j5  j	  j�  jl  j�  j�  j^  j2  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j�  Ks��R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.