���*      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Institutions�h]�h	�Text����Institutions�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�?C:\Sphinx_Learning\kib-admin-guide\docs\source\institutions.rst�hKubh	�	paragraph���)��}�(h�6This module focuses on institutions registered on KIB.�h]�h�6This module focuses on institutions registered on KIB.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�Existing Institutions�h]�h�Existing Institutions�����}�(hh@hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh=hhhh,hKubh.)��}�(h�UBy default, navigating to this module renders a table of current institutions on KIB.�h]�h�UBy default, navigating to this module renders a table of current institutions on KIB.�����}�(hhNhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh=hhubh	�block_quote���)��}�(h��..  figure::  ../../images/Institutions.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 44: Table of institutions*

�h]�h	�figure���)��}�(hhh]�(h	�image���)��}�(h��..  figure::  ../../images/Institutions.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 44: Table of institutions*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/Institutions.png��
candidates�}��*�hwsuh+hghhdhh,hK ubh	�caption���)��}�(h�"*Figure 44: Table of institutions*�h]�h	�emphasis���)��}�(hhh]�h� Figure 44: Table of institutions�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh}ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hKhhdubeh}�(h!]��id1�ah#]�h%]�h']�h)]��align��center�uh+hbhKhh^ubah}�(h!]�h#]�h%]�h']�h)]�uh+h\hh,hK	hh=hhubeh}�(h!]��existing-institutions�ah#]�h%]��existing institutions�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Creating New Institutions�h]�h�Creating New Institutions�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubh.)��}�(hX"  To create an institution, select the 'Add Institution' option located on the top right of the table of institutions. This provides a form with inputs corresponding to the details required to establish a new institution. Depicted below is a snippet of the input form provided for this cause.�h]�hX&  To create an institution, select the ‘Add Institution’ option located on the top right of the table of institutions. This provides a form with inputs corresponding to the details required to establish a new institution. Depicted below is a snippet of the input form provided for this cause.�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�hhubh])��}�(h��..  figure::  ../../images/AddInstitution.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 45: Creating an institution*

�h]�hc)��}�(hhh]�(hh)��}�(h��..  figure::  ../../images/AddInstitution.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 45: Creating an institution*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/AddInstitution.png�hx}�hzh�suh+hghh�hh,hK ubh|)��}�(h�$*Figure 45: Creating an institution*�h]�h�)��}�(hh�h]�h�"Figure 45: Creating an institution�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hKhh�ubeh}�(h!]��id2�ah#]�h%]�h']�h)]�h��center�uh+hbhKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h\hh,hKhh�hhubeh}�(h!]��creating-new-institutions�ah#]�h%]��creating new institutions�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�$Activating/Deactivating Institutions�h]�h�$Activating/Deactivating Institutions�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj  hhhh,hKubh.)��}�(hX�  Once an institution has been registered, it needs to be set to an active state.
- To activate a registered institution, click the orange toggle button in the Actions column of the table. The toggle button turns green to signify that the institution has been activated. Conversely, to deactivate a specific institution, click the green toggle button. This will turn it back to orange to signify an inactive state.�h]�hX�  Once an institution has been registered, it needs to be set to an active state.
- To activate a registered institution, click the orange toggle button in the Actions column of the table. The toggle button turns green to signify that the institution has been activated. Conversely, to deactivate a specific institution, click the green toggle button. This will turn it back to orange to signify an inactive state.�����}�(hj!  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhj  hhubeh}�(h!]��$activating-deactivating-institutions�ah#]�h%]��$activating/deactivating institutions�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Updating Institutions�h]�h�Updating Institutions�����}�(hj:  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj7  hhhh,hK$ubh.)��}�(h��To modify the details of a registered institution, select the pen-like icon against the event of interest. Modify the input fields in the resulting form as needed and then select the 'save' option at the bottom right.�h]�h��To modify the details of a registered institution, select the pen-like icon against the event of interest. Modify the input fields in the resulting form as needed and then select the ‘save’ option at the bottom right.�����}�(hjH  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK%hj7  hhubh])��}�(h��..  figure::  ../../images/InvestmentUpdateForm.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 46: Form to update details of an institution*

�h]�hc)��}�(hhh]�(hh)��}�(h��..  figure::  ../../images/InvestmentUpdateForm.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 46: Form to update details of an institution*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��%../../images/InvestmentUpdateForm.png�hx}�hzjk  suh+hghjZ  hh,hK ubh|)��}�(h�5*Figure 46: Form to update details of an institution*�h]�h�)��}�(hjo  h]�h�3Figure 46: Form to update details of an institution�����}�(hjq  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjm  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hK-hjZ  ubeh}�(h!]��id3�ah#]�h%]�h']�h)]�h��center�uh+hbhK-hjV  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h\hh,hK(hj7  hhubeh}�(h!]��updating-institutions�ah#]�h%]��updating institutions�ah']�h)]�uh+h
hhhhhh,hK$ubh)��}�(hhh]�(h)��}�(h�Deleting Institutions�h]�h�Deleting Institutions�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK1ubh.)��}�(h�mTo delete an existing institution, select the bin-like icon against the specific event in the Actions column.�h]�h�mTo delete an existing institution, select the bin-like icon against the specific event in the Actions column.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK2hj�  hhubeh}�(h!]��deleting-institutions�ah#]�h%]��deleting institutions�ah']�h)]�uh+h
hhhhhh,hK1ubh)��}�(hhh]�(h)��}�(h�Broadcasting Institutions�h]�h�Broadcasting Institutions�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK6ubh.)��}�(h�`To broadcast a specific institution, select the megaphone icon against it in the Actions column.�h]�h�`To broadcast a specific institution, select the megaphone icon against it in the Actions column.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK7hj�  hhubeh}�(h!]��broadcasting-institutions�ah#]�h%]��broadcasting institutions�ah']�h)]�uh+h
hhhhhh,hK6ubh)��}�(hhh]�(h)��}�(h�Logs of Institutions�h]�h�Logs of Institutions�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK;ubh.)��}�(h�SRetrieving logs of a specific institution can be achieved via the scroll-like icon.�h]�h�SRetrieving logs of a specific institution can be achieved via the scroll-like icon.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK<hj�  hhubeh}�(h!]��logs-of-institutions�ah#]�h%]��logs of institutions�ah']�h)]�uh+h
hhhhhh,hK;ubeh}�(h!]��institutions�ah#]�h%]��institutions�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j:  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j  j  h�h�j  j
  j4  j1  j�  j�  j�  j�  j�  j�  j  j	  u�	nametypes�}�(j  �h��j  �j4  �j�  �j�  �j�  �j  �uh!}�(j  hh�h=j
  h�j1  j  j�  j7  j�  j�  j�  j�  j	  j�  h�hdh�h�j�  jZ  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�jH  Ks��R��parse_messages�]�h	�system_message���)��}�(hhh]�(h.)��}�(h�Title underline too short.�h]�h�Title underline too short.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hj�  ubh	�literal_block���)��}�(h�*Existing Institutions
--------------------�h]�h�*Existing Institutions
--------------------�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve�uh+j�  hj�  hh,ubeh}�(h!]�h#]�h%]�h']�h)]��level�K�type��WARNING��line�K�source�h,uh+j�  hh=hhhh,hKuba�transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.