���=      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Users�h]�h	�Text����Users�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�8C:\Sphinx_Learning\kib-admin-guide\docs\source\users.rst�hKubh	�	paragraph���)��}�(h��This component mainly focuses on Create, Update and Delete (CRUD) operations that system administrators can effect on user accounts.�h]�h��This component mainly focuses on Create, Update and Delete (CRUD) operations that system administrators can effect on user accounts.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�Existing Users�h]�h�Existing Users�����}�(hh@hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh=hhhh,hKubh.)��}�(h�NBy default, navigating to this module renders a table of registered KIB users.�h]�h�NBy default, navigating to this module renders a table of registered KIB users.�����}�(hhNhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh=hhubh	�block_quote���)��}�(h��..  figure::  ../../images/ShowUsers.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 12: Existing users*

�h]�h	�figure���)��}�(hhh]�(h	�image���)��}�(h��..  figure::  ../../images/ShowUsers.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 12: Existing users*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/ShowUsers.png��
candidates�}��*�hwsuh+hghhdhh,hK ubh	�caption���)��}�(h�*Figure 12: Existing users*�h]�h	�emphasis���)��}�(hhh]�h�Figure 12: Existing users�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh}ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hKhhdubeh}�(h!]��id1�ah#]�h%]�h']�h)]��align��center�uh+hbhKhh^ubah}�(h!]�h#]�h%]�h']�h)]�uh+h\hh,hK	hh=hhubeh}�(h!]��existing-users�ah#]�h%]��existing users�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Creating New Users�h]�h�Creating New Users�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubh.)��}�(h��To create a new user, select the 'Add User' option located on the top right of the user table rendered. This provides a form with inputs corresponding to the registration details required.�h]�h��To create a new user, select the ‘Add User’ option located on the top right of the user table rendered. This provides a form with inputs corresponding to the registration details required.�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�hhubh])��}�(h��..  figure::  ../../images/CreateUserForm.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 13: Creating new user*
�h]�hc)��}�(hhh]�(hh)��}�(h��..  figure::  ../../images/CreateUserForm.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 13: Creating new user*
�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/CreateUserForm.png�hx}�hzh�suh+hghh�hh,hK ubh|)��}�(h�*Figure 13: Creating new user*�h]�h�)��}�(hh�h]�h�Figure 13: Creating new user�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hKhh�ubeh}�(h!]��id2�ah#]�h%]�h']�h)]�h��center�uh+hbhKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h\hh,hKhh�hhubh.)��}�(h�gOnce all the input fields have been populated, click the 'save' button on the bottom right of the form.�h]�h�kOnce all the input fields have been populated, click the ‘save’ button on the bottom right of the form.�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�hhubeh}�(h!]��creating-new-users�ah#]�h%]��creating new users�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Account Activation/Deactivation�h]�h�Account Activation/Deactivation�����}�(hj!  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj  hhhh,hK ubh.)��}�(hXx  When a user registers to join KIB, their account remains inactive by default as approval has to be granted by the system administrator.
Upon successful registration, the system administrator receives a notification to activate the new user account for it to be operational. In the same respect, if a system administrator creates a new user account, they need to activate them.�h]�hXx  When a user registers to join KIB, their account remains inactive by default as approval has to be granted by the system administrator.
Upon successful registration, the system administrator receives a notification to activate the new user account for it to be operational. In the same respect, if a system administrator creates a new user account, they need to activate them.�����}�(hj/  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK!hj  hhubh	�bullet_list���)��}�(hhh]�h	�	list_item���)��}�(hX�  To activate a specific user account, identify it from the table of users and click the orange icon to toggle its left dot-like feature towards the left. This will have the effect of turning it green and the user account will be activated. Deactivating an account would follow the same step, causing the icon to revert to orange.

  ..  figure::  ../../images/ActivateAcc.png
      :class: with-border
      :alt: Logs
      :align: center

      *Figure 14: Activating user account*

�h]�(h.)��}�(hXH  To activate a specific user account, identify it from the table of users and click the orange icon to toggle its left dot-like feature towards the left. This will have the effect of turning it green and the user account will be activated. Deactivating an account would follow the same step, causing the icon to revert to orange.�h]�hXH  To activate a specific user account, identify it from the table of users and click the orange icon to toggle its left dot-like feature towards the left. This will have the effect of turning it green and the user account will be activated. Deactivating an account would follow the same step, causing the icon to revert to orange.�����}�(hjH  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK$hjD  ubh])��}�(h��..  figure::  ../../images/ActivateAcc.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 14: Activating user account*

�h]�hc)��}�(hhh]�(hh)��}�(h��..  figure::  ../../images/ActivateAcc.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 14: Activating user account*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/ActivateAcc.png�hx}�hzjk  suh+hghjZ  hh,hK ubh|)��}�(h�$*Figure 14: Activating user account*�h]�h�)��}�(hjo  h]�h�"Figure 14: Activating user account�����}�(hjq  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjm  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hK+hjZ  ubeh}�(h!]��id3�ah#]�h%]�h']�h)]�h��center�uh+hbhK+hjV  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h\hh,hK&hjD  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+jB  hj?  hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]��bullet��-�uh+j=  hh,hK$hj  hhubeh}�(h!]��account-activation-deactivation�ah#]�h%]��account activation/deactivation�ah']�h)]�uh+h
hhhhhh,hK ubh)��}�(hhh]�(h)��}�(h�Updating User Accounts�h]�h�Updating User Accounts�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK/ubh.)��}�(hXx  To modify the details of a specific user account, identify it from the table of users and select the green pen-like icon in the Actions column. This renders an editable form with the details of the specified user, allowing changes to be made as deemed fit. Once changes have been made, the 'save' button at the bottom right of the form should be clicked to effect the updates.�h]�hX|  To modify the details of a specific user account, identify it from the table of users and select the green pen-like icon in the Actions column. This renders an editable form with the details of the specified user, allowing changes to be made as deemed fit. Once changes have been made, the ‘save’ button at the bottom right of the form should be clicked to effect the updates.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK0hj�  hhubh])��}�(hX  ..  figure::  ../../images/UpdateAcc.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 15: Updating user account*


..  figure::  ../../images/UserUpdateForm.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 16: User update form*

�h]�(hc)��}�(hhh]�(hh)��}�(h��..  figure::  ../../images/UpdateAcc.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 15: Updating user account*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/UpdateAcc.png�hx}�hzj�  suh+hghj�  hh,hK ubh|)��}�(h�"*Figure 15: Updating user account*�h]�h�)��}�(hj�  h]�h� Figure 15: Updating user account�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hK7hj�  ubeh}�(h!]��id4�ah#]�h%]�h']�h)]�h��center�uh+hbhK7hj�  ubhc)��}�(hhh]�(hh)��}�(h��..  figure::  ../../images/UserUpdateForm.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 16: User update form*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/UserUpdateForm.png�hx}�hzj  suh+hghj�  hh,hK ubh|)��}�(h�*Figure 16: User update form*�h]�h�)��}�(hj  h]�h�Figure 16: User update form�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hK?hj�  ubeh}�(h!]��id5�ah#]�h%]�h']�h)]�h��center�uh+hbhK?hj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h\hh,hK2hj�  hhubeh}�(h!]��updating-user-accounts�ah#]�h%]��updating user accounts�ah']�h)]�uh+h
hhhhhh,hK/ubh)��}�(hhh]�(h)��}�(h�Deleting User Accounts�h]�h�Deleting User Accounts�����}�(hj@  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj=  hhhh,hKCubh.)��}�(h�dTo delete a specific user account, locate from the table of users then select the red bin-like icon.�h]�h�dTo delete a specific user account, locate from the table of users then select the red bin-like icon.�����}�(hjN  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKDhj=  hhubh])��}�(h��..  figure::  ../../images/DelUser.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 17: Deletion icon*

�h]�hc)��}�(hhh]�(hh)��}�(h��..  figure::  ../../images/DelUser.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 17: Deletion icon*

�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/DelUser.png�hx}�hzjq  suh+hghj`  hh,hK ubh|)��}�(h�*Figure 17: Deletion icon*�h]�h�)��}�(hju  h]�h�Figure 17: Deletion icon�����}�(hjw  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hjs  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hKKhj`  ubeh}�(h!]��id6�ah#]�h%]�h']�h)]�h��center�uh+hbhKKhj\  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h\hh,hKFhj=  hhubeh}�(h!]��deleting-user-accounts�ah#]�h%]��deleting user accounts�ah']�h)]�uh+h
hhhhhh,hKCubh)��}�(hhh]�(h)��}�(h�	User Logs�h]�h�	User Logs�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKOubh.)��}�(hX]  As described in the System Logs section, information tracked by the system (logs) can be obtained. The user logs in the Users' section simply showcases the type and date of an action effected by the specified user while interacting with the system.
- To extract the logs of a specific user, locate them from the table and click the scroll-like icon.�h]�hX_  As described in the System Logs section, information tracked by the system (logs) can be obtained. The user logs in the Users’ section simply showcases the type and date of an action effected by the specified user while interacting with the system.
- To extract the logs of a specific user, locate them from the table and click the scroll-like icon.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKPhj�  hhubh])��}�(hX  ..  figure::  ../../images/UserLogIcon.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 18: User logs icon*

..  figure::  ../../images/UserLog.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 19: User logs*�h]�(hc)��}�(hhh]�(hh)��}�(h��..  figure::  ../../images/UserLogIcon.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 18: User logs icon*
�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/UserLogIcon.png�hx}�hzj�  suh+hghj�  hh,hK ubh|)��}�(h�*Figure 18: User logs icon*�h]�h�)��}�(hj�  h]�h�Figure 18: User logs icon�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hKXhj�  ubeh}�(h!]��id7�ah#]�h%]�h']�h)]�h��center�uh+hbhKXhj�  ubhc)��}�(hhh]�(hh)��}�(h�|..  figure::  ../../images/UserLog.png
    :class: with-border
    :alt: Logs
    :align: center

    *Figure 19: User logs*�h]�h}�(h!]�h#]��with-border�ah%]�h']�h)]��alt��Logs��uri��../../images/UserLog.png�hx}�hzj  suh+hghj�  hh,hK ubh|)��}�(h�*Figure 19: User logs*�h]�h�)��}�(hj
  h]�h�Figure 19: User logs�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hh,hK_hj�  ubeh}�(h!]��id8�ah#]�h%]�h']�h)]�h��center�uh+hbhK_hj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h\hh,hKShj�  hhubeh}�(h!]��	user-logs�ah#]�h%]��	user logs�ah']�h)]�uh+h
hhhhhh,hKOubeh}�(h!]��users�ah#]�h%]��users�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j`  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j:  j7  h�h�j  j  j�  j�  j:  j7  j�  j�  j2  j/  u�	nametypes�}�(j:  �h��j  �j�  �j:  �j�  �j2  �uh!}�(j7  hh�h=j  h�j�  j  j7  j�  j�  j=  j/  j�  h�hdh�h�j�  jZ  j�  j�  j)  j�  j�  j`  j�  j�  j!  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�jn  Ks��R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.