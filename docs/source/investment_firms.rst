Investment Firms
================
This module focuses on investment firms on KIB.

Existing Events
-----------------
By default, navigating to this module renders a table of current investment firms on KIB. 

    ..  figure::  ../../images/InvestmentFirms.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 44: Table of investment firms*


Creating New Investment Firms
------------------------------
To create an investment firm, select the 'Add Firm' option located on the top right of the table of investment firms. This provides a form with inputs corresponding to the details required to establish an upcoming event. Depicted below is a snippet of the input form provided for this cause.

    ..  figure::  ../../images/AddInvestmentFirm.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 45: Creating an investment firm*


Activating/Deactivating Investment Firm
---------------------------------------
Once an investment firm has been created, it needs to be set to an active state. 
- To activate an investment firm, click the orange toggle button in the Actions column of the table. The toggle button turns green to signify that the event has been activated. Conversely, to deactivate a specific event, click the green toggle button. This will turn it back to orange to signify an inactive state.


Updating Investment Firms
-------------------------
To modify an established investment firm on KIB, select the pen-like icon against the event of interest. Modify the input fields in the resulting form as needed and then select the 'save' option at the bottom right.


    ..  figure::  ../../images/InvestmentUpdateForm.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 46: Form to update an investment firm*


Deleting Investment Firms
-------------------------
To delete an existing investment firm, select the bin-like icon against the specific event in the Actions column.


Broadcasting Investment Firms
-----------------------------
To broadcast a specific investment firm, select the megaphone icon against it in the Actions column.


Logs of Investment Firms
------------------------
Retrieving logs of a specific investment firm can be achieved via the scroll-like icon.