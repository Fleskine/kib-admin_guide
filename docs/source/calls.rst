Calls
=====
This module enables publishing of messages that seek users to swing into action in alignment to a specified objective. 

Existing Calls
--------------
By default, navigating to this module renders a table of current calls on KIB. Calls are categorized into:

#. Open Calls: These are calls that are active and still accepting responses.
#. Closed Calls: These are calls that are no longer actively receiving responses.


    ..  figure::  ../../images/CallsList.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 20: Calls table*


Creating New Calls
------------------
To create a new call, select the 'Add Call' option located on the top right of the calls table. This provides a form with inputs corresponding to the details required to establish a new call. Depicted below is a snippet of the input form provided for this cause.

    ..  figure::  ../../images/AddCall.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 21: Creating a call*


Activating/Deactivating Calls
-----------------------------
Once a call has been created, it needs to be set to an active state for it to accept responses. 
- To activate a specific call, click the orange toggle button in the Actions column of the table. The toggle button turns green to signify that the call has been activated. Conversely, to deactivate a specific call, click the green toggle button. This will turn it back to orange to signify an inactive state.

    ..  figure::  ../../images/ActivateCall.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 22: Activating/Deactivating a call*


Updating Calls
--------------
To modify an existing call, select the pen-like icon against the desired call. Modify the input fields in the resulting form as needed and then select the 'save' option at the bottom right.

    ..  figure::  ../../images/UpdateCall.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 23: Update icon*


    ..  figure::  ../../images/CallUpdateForm.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 24: Form to update call*


Deleting Calls
--------------
To delete a specific call from the table, select the bin-like icon against the specific call in the Actions column.

    ..  figure::  ../../images/CallDel.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 25: Deletion icon*


Broadcasting Calls
------------------
To broadcast a specific call, select the megaphone icon against it in the Actions column.

    ..  figure::  ../../images/CallBroadcast.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 26: Broadcasting icon*


Call Logs
---------
To view the logs of a specific call, select the scroll-like icon.

    ..  figure::  ../../images/CallLogs.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 27: Logs icon*