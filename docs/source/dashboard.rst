Dashboard
=========
This user interface aims at providing a summary of most entities that interact with the system and is accessible via the 'Administration' dropdown. Reporting the states of various system components is enhanced with visualizations such as bar graphs and charts. Some of these components include:

#. Innovations
#. Users
#. Institutions
#. Investment Firms
#. Events
#. Calls
#. Funding Requests

Inspecting any of the components renders a bar graph and a pie chart on the lower left and right side respectively. The illustrations below provide a summary of innovations.


    ..  figure::  ../../images/SummaryCards.png
        :class: with-border
        :alt: System components
        :align: center

        *Figure 1: State summary of system components*

        
    ..  figure::  ../../images/InnovationsGraphChart.png
        :class: with-border
        :alt: Bar graph & pie chart of innovations
        :align: center

        *Figure 2: Summary plots of innovations*

