Organizations
=============
This module focuses on organizations registered on KIB.

Existing Organizations
----------------------
By default, navigating to this module renders a table of current organizations on KIB. 

    ..  figure::  ../../images/Organizations.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 47: Table of organizations*


Creating New Organizations
--------------------------
To create an organization, select the 'Add Organization' option located on the top right of the table of organizations. This provides a form with inputs corresponding to the details required to establish a new organization. Depicted below is a snippet of the input form provided for this cause.

    ..  figure::  ../../images/AddOrg.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 48: Creating an organization*


Activating/Deactivating Organizations
-------------------------------------
Once an organization has been registered, it needs to be set to an active state. 
- To activate a registered organization, click the orange toggle button in the Actions column of the table. The toggle button turns green to signify that the organization has been activated. Conversely, to deactivate a specific organization, click the green toggle button. This will turn it back to orange to signify an inactive state.


Updating Organizations
----------------------
To modify the details of a registered organization, select the pen-like icon against the event of interest. Modify the input fields in the resulting form as needed and then select the 'save' option at the bottom right.


    ..  figure::  ../../images/UpdateOrg.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 49: Form to update details of an organization*


Deleting Organizations
----------------------
To delete an existing organization, select the bin-like icon against the specific event in the Actions column.


Broadcasting Organizations
--------------------------
To broadcast a specific organization, select the megaphone icon against it in the Actions column.


Logs of Organizations
---------------------
Retrieving logs of a specific organization can be achieved via the scroll-like icon.