Institutions
============
This module focuses on institutions registered on KIB.

Existing Institutions
--------------------
By default, navigating to this module renders a table of current institutions on KIB. 

    ..  figure::  ../../images/Institutions.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 44: Table of institutions*


Creating New Institutions
-------------------------
To create an institution, select the 'Add Institution' option located on the top right of the table of institutions. This provides a form with inputs corresponding to the details required to establish a new institution. Depicted below is a snippet of the input form provided for this cause.

    ..  figure::  ../../images/AddInstitution.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 45: Creating an institution*


Activating/Deactivating Institutions
------------------------------------
Once an institution has been registered, it needs to be set to an active state. 
- To activate a registered institution, click the orange toggle button in the Actions column of the table. The toggle button turns green to signify that the institution has been activated. Conversely, to deactivate a specific institution, click the green toggle button. This will turn it back to orange to signify an inactive state.


Updating Institutions
---------------------
To modify the details of a registered institution, select the pen-like icon against the event of interest. Modify the input fields in the resulting form as needed and then select the 'save' option at the bottom right.


    ..  figure::  ../../images/InvestmentUpdateForm.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 46: Form to update details of an institution*


Deleting Institutions
---------------------
To delete an existing institution, select the bin-like icon against the specific event in the Actions column.


Broadcasting Institutions
-------------------------
To broadcast a specific institution, select the megaphone icon against it in the Actions column.


Logs of Institutions
--------------------
Retrieving logs of a specific institution can be achieved via the scroll-like icon.