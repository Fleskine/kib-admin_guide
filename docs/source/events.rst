Events
======
This module bears events organized to foster innovations as is the goal of KIB.

Existing Events
-----------------
By default, navigating to this module renders a table of current events on KIB. These events are categorized into:

#. Open Events: These are events that are upcoming and are open to invitations.
#. Closed Events: These are events that are no longer open to invitations.

    ..  figure::  ../../images/EventsTable.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 36: Table of events*


Creating New Events
-------------------
To create an upcoming event, select the 'Add Event' option located on the top right of the Events table. This provides a form with inputs corresponding to the details required to establish an upcoming event. Depicted below is a snippet of the input form provided for this cause.

    ..  figure::  ../../images/AddEvent.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 37: Creating an upcoming event*


Activating/Deactivating Events
------------------------------
Once an upcoming event has been created, it needs to be set to an active state for it to be open to invitations. 
- To activate a specific event, click the orange toggle button in the Actions column of the table. The toggle button turns green to signify that the event has been activated. Conversely, to deactivate a specific event, click the green toggle button. This will turn it back to orange to signify an inactive state.

    ..  figure::  ../../images/ActivateCall.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 38: Activating/Deactivating an event*


Updating Events
---------------
To modify an established event on KIB, select the pen-like icon against the event of interest. Modify the input fields in the resulting form as needed and then select the 'save' option at the bottom right.

    ..  figure::  ../../images/UpdateCall.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 39: Update icon*


    ..  figure::  ../../images/EventUpdateForm.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 40: Form to update an event*


Deleting Events
---------------
To delete a specific event from the table of events, select the bin-like icon against the specific event in the Actions column.

    ..  figure::  ../../images/DelUser.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 41: Deletion icon*


Broadcasting Events
-------------------
To broadcast a specific event, select the megaphone icon against it in the Actions column.

    ..  figure::  ../../images/BroadcastIcon.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 42: Broadcasting icon*


Events Logs
-----------
To view the logs of a specific event, select the scroll-like icon.

    ..  figure::  ../../images/LogIcon.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 43: Logs icon*