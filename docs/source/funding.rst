Funding Requests
================
This module includes requests for funding made by innovators. 

Existing Requests
-----------------
By default, navigating to this module renders a table of current funding requests on KIB. These requests are categorized into:

#. Open Asks: These are requests that are active and still accepting responses.
#. Closed Asks: These are requests that are no longer actively receiving responses.

    ..  figure::  ../../images/FundingRequestsTable.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 28: Table of funding requests*


Creating New Requests
---------------------
To create a new funding request, select the 'Add Ask' option located on the top right of the Asks table. This provides a form with inputs corresponding to the details required to establish a new funding request. Depicted below is a snippet of the input form provided for this cause.

    ..  figure::  ../../images/FundingRequestForm.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 29: Creating a funding request*


Activating/Deactivating Requests
--------------------------------
Once a funding request has been created, it needs to be set to an active state for it to accept responses. 
- To activate a specific funding request, click the orange toggle button in the Actions column of the table. The toggle button turns green to signify that the funding request has been activated. Conversely, to deactivate a specific funding request, click the green toggle button. This will turn it back to orange to signify an inactive state.

    ..  figure::  ../../images/ActivateCall.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 30: Activating/Deactivating a funding request*


Updating Funding Requests
-------------------------
To modify an existing funding request, select the pen-like icon against the desired funding request. Modify the input fields in the resulting form as needed and then select the 'save' option at the bottom right.

    ..  figure::  ../../images/UpdateCall.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 31: Update icon*


    ..  figure::  ../../images/CallUpdateForm.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 32: Form to update funding request*


Deleting Funding Requests
-------------------------
To delete a specific funding request from the table, select the bin-like icon against the specific call in the Actions column.

    ..  figure::  ../../images/DelUser.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 33: Deletion icon*


Broadcasting Funding Requests
-----------------------------
To broadcast a specific funding request, select the megaphone icon against it in the Actions column.

    ..  figure::  ../../images/BroadcastIcon.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 34: Broadcasting icon*


Funding Requests Logs
---------------------
To view the logs of a specific funding request, select the scroll-like icon.

    ..  figure::  ../../images/LogIcon.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 35: Logs icon*