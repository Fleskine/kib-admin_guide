Reports
=======
This component of the application facilitates analysis of various system entities such as user accounts and innovations. It permits the system administrator to filter information as required.
On this page, the system admin can inspect Users, Innovations and Computed Reports. These options will be visible in a window located on the top left.

    ..  figure::  ../../images/ReportAspects.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 4: User account report filters*

Users
-----
The system administrator can check the status of user accounts registered on KIB. For instance, the system administrator can run inspections based on active or inactive accounts. Once the filters of interest are selected, the 'search' button situated at the bottom right of the selection window may be clicked to fetch results accordingly. The illustration below depicts various filters that can be used to report on the status of user accounts.


    ..  figure::  ../../images/UsersReportFilters.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 5: User account report filters*

If the filter-based search yields results, the records satisfying the search will be returned and populated in the table that renders at the bottom as shown.

    ..  figure::  ../../images/ReportsTable.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 6: User account report filters*

On the other hand, if no records satisfy the filters specified, then the system administrator will be informed that no user was found.


Innovations
-----------
The system administrator can check the status of innovations registered on KIB. The records returned may be filtered based on:

#. County.
#. Innovation Stage.
#. IP Protection Type.
#. Approval Status.
#. Dates.
#. Innovations with offers or those without.
#. Interests.
#. Sectors.
#. SDGs.

    ..  figure::  ../../images/InnovationFilters.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 7: Innovation filters*

The Interests, Sectors and SDGs filters are contained within the 'Show more Filters' option. To reveal them, the system administrator should check the checkbox.

    ..  figure::  ../../images/InnovationFilters2.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 8: More Innovation filters*


Computed Reports
----------------
This aspect of the Reports Module includes bar graph plots that provide information pertaining to user interests, connections and trends.