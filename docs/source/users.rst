Users
=====
This component mainly focuses on Create, Update and Delete (CRUD) operations that system administrators can effect on user accounts.

Existing Users
--------------
By default, navigating to this module renders a table of registered KIB users.

    ..  figure::  ../../images/ShowUsers.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 12: Existing users*


Creating New Users
------------------
To create a new user, select the 'Add User' option located on the top right of the user table rendered. This provides a form with inputs corresponding to the registration details required.

    ..  figure::  ../../images/CreateUserForm.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 13: Creating new user*

Once all the input fields have been populated, click the 'save' button on the bottom right of the form.


Account Activation/Deactivation
-------------------------------
When a user registers to join KIB, their account remains inactive by default as approval has to be granted by the system administrator. 
Upon successful registration, the system administrator receives a notification to activate the new user account for it to be operational. In the same respect, if a system administrator creates a new user account, they need to activate them.

- To activate a specific user account, identify it from the table of users and click the orange icon to toggle its left dot-like feature towards the left. This will have the effect of turning it green and the user account will be activated. Deactivating an account would follow the same step, causing the icon to revert to orange.   

    ..  figure::  ../../images/ActivateAcc.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 14: Activating user account*


Updating User Accounts
----------------------
To modify the details of a specific user account, identify it from the table of users and select the green pen-like icon in the Actions column. This renders an editable form with the details of the specified user, allowing changes to be made as deemed fit. Once changes have been made, the 'save' button at the bottom right of the form should be clicked to effect the updates.

    ..  figure::  ../../images/UpdateAcc.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 15: Updating user account*


    ..  figure::  ../../images/UserUpdateForm.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 16: User update form*


Deleting User Accounts
----------------------
To delete a specific user account, locate from the table of users then select the red bin-like icon.

    ..  figure::  ../../images/DelUser.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 17: Deletion icon*


User Logs
---------
As described in the System Logs section, information tracked by the system (logs) can be obtained. The user logs in the Users' section simply showcases the type and date of an action effected by the specified user while interacting with the system.
- To extract the logs of a specific user, locate them from the table and click the scroll-like icon.

    ..  figure::  ../../images/UserLogIcon.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 18: User logs icon*

    ..  figure::  ../../images/UserLog.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 19: User logs*