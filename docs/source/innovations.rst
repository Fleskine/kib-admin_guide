Innovations
===========
This module focuses on innovations posted on KIB.

Existing Innovations
--------------------
By default, navigating to this module renders a table of current innovations on KIB. Based on the status, innovations are majorly grouped into 2 types:

#. Active 
#. Inactive


    ..  figure::  ../../images/InnovationsTable.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 50: Table of innovations*


Approving/Declining innovations
-------------------------------
Once innovations have been created by innovators, they require approval from the system administrator. The system administrator will be notified in the event that an innovator registers their innovation on KIB.

- To read the notification, select the bell icon towards the top right. It will reveal a pop-up window bearing the latest unread messages. If there exists a message relating to creation of a new innovation, select it and provide approval in the resulting window.

    ..  figure::  ../../images/InnovationsTable.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 51: Notifications*

    ..  figure::  ../../images/InnovationMessage.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 52: Innovation notification*

    ..  figure::  ../../images/InnovationsTable.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 53: Innovation approval form*

- The window that renders for approval necessitates that the system administrator provides a message that will be relayed to the innovator alongside the approval or decline sent.

- An innovation may also be approved without necessitating a message from the system administrator by selecting the toggle icon in the Actions column just as in the other modules.