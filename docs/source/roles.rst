Roles
=====
Roles are linked to permissions which determine access levels assigned to users thus determining actions that can be performed by each user while interacting with the system. Some of the roles present in KIB include:

#. Institution Admin
#. Organization Admin
#. Investment Firm Admin
#. Innovation Admin
#. System Admin
#. ESO Admin
#. Training Role
#. Innovator
#. Investor
#. Researcher
#. Enthusiast


Existing Roles
--------------
By default, navigating to the Roles section renders a table of existing roles as shown below.

    ..  figure::  ../../images/RolesTable.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 9: Existing roles*


Creating New Roles
------------------
To create a new role, select the 'Add Role' option that is located on the top right of the display table. This renders a form with input fields that are necessary for the inclusion of a role.

    ..  figure::  ../../images/RolesPerm.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 10: Creating a new role*

The left window within the form provides a list of all present permissions that can be associated with the new role that is to be created whereas the right window indicates the permissions selected for the new role. 

- To assign a single permission to the role being created, click on a specific permission from the left window and then click the button with a greater than symbol **(>)**. On the other hand, to assign all the available permissions to the role being created, select the double greater than symbol **(>>)**.
- Similarly, to withdraw a single permission assigned to a role, select the permission to withdraw from the right window and click the button with a less than symbol **(<)**. To withdraw all permissions assigned to the role being created, select the button with two less than symbols **(<<)**.
- Finally, the 'save' button at the bottom right of the update form should then be clicked to fulfill creation of the role.
    
    ..  figure::  ../../images/RoleAssignmentButtons.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 11: Creating a new role*


Updating Existing Roles
-----------------------
To update an existing role, select the green pen icon in the Actions column for the specific role that is to be modified. This renders a similar form as the one used for creating a new form. Permissions can then be assigned or withdrawn just as those followed when defining a new role. The 'save' button at the bottom right of the update form should then be clicked to commit the changes. 