.. KIB Admin Guide documentation master file, created by
   sphinx-quickstart on Tue Jul 18 09:52:00 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KIB Admin Guide's documentation!
===========================================
Kenya Innovation Bridge helps in the discoverability of innovations in Kenya. 
The platform is a marketplace that enables innovators, inventors, researchers 
and startups to introduce their solutions to partners, funders, customers and users.
The goal is to get as many innovations to scale by attracting appropriate funding and partnerships.
This user guide highlights the basic operations that can be performed by end users as they
interact with the system.

This user manual is aimed at providing guidance particularly to system administrators to aid in management of'
the system.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   dashboard
   logs
   reports
   roles
   users
   calls
   funding
   events
   investment_firms
   institutions
   organizations
   innovations
