System Logs
===========
The Logs module provides details of various changes that are tracked as users interact with the system. Some of these details include:

#. Users.
#. Model - this indicates the backend methods invoked for various operations initiated while interacting with the system.
#. Action - the nature of the operation performed while interacting with the system (Create, Update and Delete).
#. Date and Time.

These System Logs can be retrieved based on specific filters which can be applied to obtain records that satisfy some desired conditions. Filters may be specified through the input fields situated at the top section of the Logs Table. The 'search' button also situated at the top section of the table should then be clicked to initiate the retrieval of logs as per the filters specified.
The illustration below depicts logs retrieved.

    ..  figure::  ../../images/Logs.png
        :class: with-border
        :alt: Logs
        :align: center

        *Figure 3: Summary of system logs*